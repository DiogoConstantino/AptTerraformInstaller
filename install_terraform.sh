#!/bin/env bash

#    This file is part of AptTerraformInstaller.
#
#    AptTerraformInstaller is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    AptTerraformInstaller is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more detsails.
#
#    You should have received a copy of the GNU General Public License
#    along with AptTerraformInstaller.  If not, see <https://www.gnu.org/licenses/>


# Uncomment for debugging
set -x

# Defaults and computations:
success=0 # 0 is ok
log="/tmp/terraform_install.log"
sys_lsb_release=$(lsb_release -cs)
deb_cpu_arch=amd64
apt_repository_str="deb [arch=${deb_cpu_arch}] https://apt.releases.hashicorp.com ${sys_lsb_release} main"
real_cpu_arch=$( lscpu | grep -q -e x86_64; echo $? )

# Add this function to all functions of main business
function exit_if_not_ok {
    # echo "success @ exit_if_not_ok: '${success}'" # helper for debugging

    if [[ ${success} -ne 0 ]]
    then
        exit ${success}
    fi
}


function allow_to_install_curl {
    exit_if_not_ok

    echo "Curl is a run time dependency for this script, do you allow us to install it? y/n"
    read -r allow

    if [[ ${allow} != 'y' ]]
    then
        succes=1
    fi

    echo ${success}
}

# Installing curl if not installed
function install_curl {
    exit_if_not_ok

    echo "Trying to find curl, as it's a run time dependency for this script)"
    which curl

    if [[ $? -ne 0 ]]
    then
        echo "Curl is currently not installed or on PATH."
        allow_to_install_curl
b       apt install curl | tee -a ${log}

        if [[ $? -ne 0 ]]
        then
            echo "Failed to install curl. Operation log at '${log}'"
            success=2
        fi
    fi

    echo ${success}
}

function config_repo {
    exit_if_not_ok
    
    curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add - | tee -a ${log}

    sudo apt-add-repository "${apt_repository_str}" | tee -a ${log}

    echo ${success}
}


function install_terraform {
    exit_if_not_ok

    sudo apt-get update && sudo apt-get install terraform | tee -a ${log}

    if [[ $? -ne 0 ]]
    then
        echo "Failed to install terraform. Operation log at '${log}'"
        success=1
    fi

    echo ${success}
}





allow_to_install_curl

# Verify cpu arch:
if [[ ${real_cpu_arch} -eq 0 ]] 
then
    # success=1 # helper for debugging
    install_curl
    config_repo
    install_terraform
else
    echo "I'm sorry, but his script can only install Terraform on x86_64/amd64 based systems."
fi

# echo "success @ end: '${success}'" # helper for debugging
exit ${success}